#include <bits/stdc++.h>
using namespace std;
struct node
{
int val;
node * children[101];
int count=0;
public:
node(int v)
{
val=v;
}
};
node * arr[101];
map<int,int>visited;
int ans=0;
int dfs(int s)
{
visited[s]=1;
node *temp=arr[s];
int node_count=0;
//cout<<endl<<"s="<<s<<endl;
for(int i=0;i<arr[s]->count;i++)
{
//cout<<arr[s]->children[i]->val<<" ";
if(!visited[arr[s]->children[i]->val])
node_count+=dfs(arr[s]->children[i]->val);
}
//cout<<"s="<<s<<"nodes="<<node_count<<endl;
if(node_count%2==1)
ans++;
return 1+node_count;
}
int main()
{
int n,m;
cin>>n>>m;

for(int i=0;i<n;i++)
arr[i]=new node(i);

for(int i=0;i<m;i++)
{
int a,b;
cin>>a>>b;
a--;
b--;
arr[a]->children[arr[a]->count++]=arr[b];
arr[b]->children[arr[b]->count++]=arr[a];
}
dfs(0);
cout<<ans-1<<endl;
}
