#include <bits/stdc++.h>
using namespace std;

vector<long long >adj[100001];
bool visited[100001];
long long  n,m,clib,croad;

long long  dfsutil(long long  src)
{
visited[src]=true;
long long  no_of_nodes=1;
for(long long  i=0;i<adj[src].size();i++)
{
if(!visited[adj[src][i]])
no_of_nodes+=dfsutil(adj[src][i]);
}
return no_of_nodes;
}
void dfs()
{
for(long long  i=0;i<100001;i++)
visited[i]=false;
long long  no_of_components=0;
long long  no_of_edges=0;

for(long long  i=0;i<n;i++)
{
if(!visited[i])
{
no_of_components++;
no_of_edges=no_of_edges+dfsutil(i)-1;
}
}
cout<<min(no_of_components*clib+no_of_edges*croad,clib*n)<<endl;
}
int main()
{
long long  q;
cin>>q;
while(q--)
{
cin>>n>>m>>clib>>croad;
for (int i = 0; i <= n; i++)
    {
            visited[i] = false;
            adj[i].clear();
        }
for(long long  i=0;i<m;i++)
{
long long  u,v;
cin>>u>>v;
adj[u-1].push_back(v-1);
adj[v-1].push_back(u-1);
}
dfs();
}
}
